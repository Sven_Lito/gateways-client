const debug = require('debug')('httpService');
const url = require('url');
const axios = require('axios');

module.exports = (cfg) => {

  const { secure, hostname, port, timeout, version } = cfg.endpoint.http;
  const protocol = secure ? 'https' : 'http';

  const baseURL = url.format({
    protocol,
    hostname,
    port: secure ? '443' : port,
    pathname: version,
    slashes: true
  });

  const instance = axios.create({
    baseURL,
    timeout
  });

  instance.interceptors.request.use((req) => {
    debug('Request:');
    debug(req.data);
    return req;
  }, (err) => {
    debug('Error:');
    debug(err);
    return Promise.reject(err);
  });

  instance.interceptors.response.use((resp) => {
    debug('Response:');
    debug(resp.data);
    return resp;
  }, (err) => {
    debug('Error:');
    debug(err);
    return Promise.reject(err);
  });


  const httpService = (endpoint, opts) => {
    return instance.request(Object.assign({
      withCredentials: true,
      url: endpoint
    }, opts))
    .then(resp => resp.data)
    .catch(error => error);
  };

  return httpService;

};
