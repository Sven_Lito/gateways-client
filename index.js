const httpService = require('./lib/http');

const GatewayClient = (props) => {
  if (!props) { throw new Error('property object required'); }

  const http = (endpoint, opts) => httpService(props)(endpoint, opts);

  return Object.freeze({
    http
  });

};

module.exports = GatewayClient;
