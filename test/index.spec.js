const GatewayClient = require('../index');
const config = require('./support/cfg');
const chai = require('chai');
const nock = require('nock');

const { expect } = chai;

describe('gateway client', () => {

  it('should throw without options', () => {
    expect(GatewayClient).to.throw(/property object required/);
  });

  it('should expose a http method', () => {
    const client = GatewayClient(config);
    expect(client).to.have.property('http');
  });

  describe('http service', () => {

    let client;

    beforeEach(() => {
      client = GatewayClient(config);
    });

    afterEach(() => {
      client = null;
    });

    it('should return a promise', () => {
      const { http } = client;
      expect(http('')).to.have.property('then');
      expect(http('')).to.have.property('catch');
    });

    it('should call then with a successful response', () => {
      nock('http://0.0.0.0:8080')
      .get('/v1/test')
      .reply(200, {
        version: '0.1.0'
      });

      const { http } = client;
      return http('/test').then((resp) => {
        expect(resp).to.be.an('object');
        expect(resp.version).to.equal('0.1.0');
      });
    });

    it('should call catch with an error message', () => {
      nock('http://0.0.0.0:8080')
      .get('/v1/test')
      .reply(400, { });

      const { http } = client;
      return http('/test').catch((err) => {
        expect(err).to.equal(true);
      });
    });

  });

});
