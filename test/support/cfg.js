const config = {
  api: {
    port: process.env.SERVICE_PORT || 3000
  },
  docs: {
    port: process.env.SERVICE_DOCS_PORT || 4000,
    path: process.env.SERVICE_DOCS_PATH || '/docs'
  },
  endpoint: {
    http: {
      secure: false,
      hostname: process.env.ESB_HOSTNAME || '0.0.0.0',
      port: process.env.ESB_PORT || 8080,
      timeout: process.env.ESB_TIMEOUT || 10000,
      version: process.env.ESB_PATH || '/v1'
    },
    mq: {
      protocol: 'amqp',
      hostname: process.env.MQ_HOSTNAME || 'esb-service',
      port: process.env.MQ_PORT || 1414,
      timeout: process.env.MQ_TIMEOUT || 10000,
      username: process.env.MQ_USER || '',
      password: process.env.MQ_PASS || ''
    },
    soap: {
      secure: false,
      hostname: process.env.ESB_HOSTNAME || 'esb-service',
      port: process.env.ESB_PORT || 8080,
      timeout: process.env.ESB_TIMEOUT || 10000
    }
  }
};

module.exports = config;
