# Jenius Gateway Client



```js
const config = {
  endpoint: {
    http: {
      secure: false,
      hostname: process.env.ESB_HOSTNAME || 'esb-service',
      port: process.env.ESB_PORT || 8080,
      timeout: process.env.ESB_TIMEOUT || 10000,
      version: process.env.ESB_PATH || '/v1'
      /* { auth, token, headers, etc. */
    }
  }
};

```



HTTP Client

```js

const gateway = GatewayClient(config);
const { http } = gateway;

http('/users/321/cards/info')
.then(res => console.log(res))
.catch(err => console.log(err));


```

